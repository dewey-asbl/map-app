import Vue from 'vue'
import Router from 'vue-router'
import Plan from '@/components/Plan'
import Home from '@/components/Home'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Home
    },

    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/plan',
      name: 'Plan',
      component: Plan
    },
    {
      path: '/plan/:cat_id',
      name: 'PlanCategory',
      component: Plan
    },
    {
      path: '/plan/:cat_id/:subcat_id',
      name: 'PlanSubcategory',
      component: Plan
    },
    {
      path: '/marker/:marker_id',
      name: 'PlanMarker',
      component: Plan
    },
  ]
})
