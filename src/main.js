// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import Vue2Leaflet from 'vue2-leaflet';
import { alert, aside, checkbox } from 'vue-strap';
import Multiselect from 'vue-multiselect'
import VueInstantSearch from 'vue-instantsearch';
import Places from 'vue-places';
import Card from './components/Card';

Vue.use(VueInstantSearch);

Vue.config.productionTip = false

Vue.component('v-map', Vue2Leaflet.Map);
Vue.component('v-tilelayer', Vue2Leaflet.TileLayer);
Vue.component('v-marker', Vue2Leaflet.Marker);
Vue.component('sidebar', aside);
Vue.component('alert', alert);
Vue.component('checkbox', checkbox);
Vue.component('places', Places);
Vue.component('multiselect', Multiselect);
Vue.component('card', Card);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
