let request = require("request");
let fs = require('fs');

url = 'http://maps.dewey.be/api/categories.json';

request(url, function (error, response, html) {

  let categories = JSON.parse(response.body);

  // Save all markups
  categories.map((item) => {

    let count = 0;

    item.subcategories.map((item) => {
      request('http://maps.dewey.be/api/subcategories/' + item.id + '/markers?format=json', (error, response, html) => {
        try {

          let markers = JSON.parse(response.body);
          fs.writeFile('./static/data/markers-' + item.id + '.json', html);
          console.log('- Subcategory #' + item.id + ' imported');

          item.count = markers.length;

          count = count + markers.length;
        } catch (e) {
          console.log('Cannot parse ' + item.id);
        }
      });

      return item;
    });

    return item;
  });

  // Write categories
  //fs.writeFile('./static/categories.json', JSON.stringify(categories));
  console.log('- Categories imported');
});

const theDirectory = './static/data';

markers = [];

fs.readdirSync(theDirectory).forEach((file) => {
  let marks = require('./static/data/' + file);

  /**
   * Add _geoloc for Algolia Instant Search
   */
  marks.map((mark) => {
    mark._geoloc = {
      lat: mark.lat,
      lng: mark.lon
    };
    mark.created_at = new Date(mark.created).getTime();
    mark.updated_at = new Date(mark.last_modified).getTime();
    return mark;
  });

  markers = markers.concat(marks);
});

fs.writeFile('./static/markers.json', JSON.stringify(markers), 'utf8');


markers = {};

let cats = require('./static/categories.json');

cats.forEach((cat) => {

  let count = 0;

  cat.subcategories.map((subcat) => {
    markers[subcat.id] = cat.id;
  });

  fs.writeFile('./static/main-cats.json', JSON.stringify(markers), 'utf8');
});
